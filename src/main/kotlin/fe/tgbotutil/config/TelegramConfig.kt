package fe.tgbotutil.config

import java.net.Authenticator
import java.net.InetSocketAddress
import java.net.PasswordAuthentication
import java.net.Proxy

data class TelegramConfig(
    val botToken: String,
    val proxyConfig: ProxyConfig?
)

data class ProxyConfig(
    val host: String,
    val port: Int,
    val type: Proxy.Type,
    val username: String?,
    val password: String?
) {
    fun hasAuth() = this.username != null && this.password != null
    fun toProxy() = Proxy(type, InetSocketAddress(host, port))

    fun setAsRequestAuth() {
        Authenticator.setDefault(object : Authenticator() {
            override fun getPasswordAuthentication() = PasswordAuthentication(username, password?.toCharArray())
        })
    }
}

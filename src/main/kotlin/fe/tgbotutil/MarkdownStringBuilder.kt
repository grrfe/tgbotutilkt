package fe.tgbotutil

import kotlin.text.StringBuilder

typealias MarkdownBuildOperation = MarkdownStringBuilder.() -> Unit
typealias SpacedBuildOperation = () -> MarkdownStringBuilder

fun buildMarkdownString(fn: MarkdownBuildOperation): String {
    return MarkdownStringBuilder().apply(fn).build()
}

fun buildMarkdown(fn: MarkdownBuildOperation): MarkdownStringBuilder {
    return MarkdownStringBuilder().apply(fn)
}

fun MarkdownStringBuilder.append(fn: MarkdownBuildOperation) = this.apply(fn)

class MarkdownStringBuilder(
    private val builder: StringBuilder = StringBuilder()
) {
    private val escape =
        listOf("_", "*", "[", "]", "(", ")", "~", "`", ">", "#", "+", "-", "=", "|", "{", "}", ".", "!")

    fun line(fn: MarkdownBuildOperation): MarkdownStringBuilder {
        return this.apply(fn).newLine()
    }

    fun spaced(vararg operations: SpacedBuildOperation): MarkdownStringBuilder {
        operations.forEachIndexed { index, function ->
            function()
            if (index != operations.size - 1) space()
        }

        return this
    }

    fun space(): MarkdownStringBuilder {
        builder.append(" ")
        return this
    }

    fun newLine(text: String): MarkdownStringBuilder {
        return line { text(text) }
    }

    fun newLine(): MarkdownStringBuilder {
        builder.append("\n")
        return this
    }

    fun text(text: String): MarkdownStringBuilder {
        builder.append(escape.fold(text) { acc, element -> acc.replace(element, "\\$element") })
        return this
    }

    fun bold(text: String): MarkdownStringBuilder {
        this.applyOp(text, "*")
        return this
    }

    fun italic(text: String): MarkdownStringBuilder {
        this.applyOp(text, "_")
        return this
    }

    fun code(text: String): MarkdownStringBuilder {
        this.applyOp(text, "`")
        return this
    }

    fun strike(text: String): MarkdownStringBuilder {
        this.applyOp(text, "~")
        return this
    }

    fun spoiler(text: String) {
        this.applyOp(text, "||")
    }

    fun underline(text: String): MarkdownStringBuilder {
        this.applyOp(text, "_")
        return this
    }

    fun inlineUrl(text: String, url: String): MarkdownStringBuilder {
        this.builder.append("[")
        text(text)
        this.builder.append("](")
        this.builder.append(url.replace(")", "\\)").replace("\\", "\\\\"))
        this.builder.append(")")

        return this
    }

    private fun applyOp(text: String, op: String): MarkdownStringBuilder {
        builder.append(op)
        text(text)
        builder.append(op)
        return this
    }

    fun build() = this.builder.toString()

    fun copy() = MarkdownStringBuilder(StringBuilder(this.builder.toString()))
}

package fe.tgbotutil

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.request.AnswerCallbackQuery
import fe.tgbotutil.command.CommandHandler
import fe.tgbotutil.ext.sendText

abstract class TelegramUpdateHandler(
    private val bot: TelegramBot,
    private val commandHandler: CommandHandler
) : UpdatesListener {

    override fun process(updates: MutableList<Update>): Int {
        try {
            updates.forEach { update ->
                val message = update.message()
                if (message != null) {
                    val replyText = commandHandler.execMessageCmd(bot, message)
                    if (replyText != null) {
                        bot.sendText(message.chat().id(), replyText)
                    }

                    messageCommandExecuted(message, replyText)
                }

                val query = update.callbackQuery()
                if (query != null) {
                    if (!TelegramCallbackHandler.execute(query)) {
                        this.noCallbackHandlerFound(query)
                    }

                    bot.execute(AnswerCallbackQuery(query.id()))
                }
            }
        } catch (e: Exception) {
            val res = handleProcessException(e)
            if (res != null) {
                return res
            }
        }

        return UpdatesListener.CONFIRMED_UPDATES_ALL
    }

    protected abstract fun handleProcessException(exception: Exception): Int?

    protected abstract fun noCallbackHandlerFound(query: CallbackQuery)
    protected abstract fun messageCommandExecuted(message: Message, returnMsg: String?)
}

package fe.tgbotutil.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup

class CommandHandler(private val commands: List<Command<*>>, private val delimiter: String = " ") {
    val cmdList by lazy {
        commands.joinToString(separator = "\n") { "${it.name} ${it.formattedUsage}: ${it.description}" }
    }

    val cmdReplyKeyboard by lazy {
        ReplyKeyboardMarkup(
            commands.map {
                it.name
            }.toTypedArray()
        )
    }

    fun execMessageCmd(bot: TelegramBot, msg: Message): String? {
        msg.text()?.let { msgText ->
            val split = msgText.split(delimiter)
            commands.forEach { cmd ->
                if (split.first() in cmd.names) {
                    val args = split.run {
                        this.subList(1, this.size)
                    }

                    if (!cmd.creator.hasEnoughArgs(args.size) || cmd.exec(args,bot, msg.from(), msg.chat().id()) == null) {
                        return "Usage: ${cmd.name} ${cmd.formattedUsage}"
                    }
                }
            }
        }

        return null
    }
}

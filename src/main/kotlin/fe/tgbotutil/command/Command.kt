package fe.tgbotutil.command

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.User
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup

abstract class Command<A>(
    val name: String, usage: String?, val description: String,
    val creator: CommandArgumentCreator<A>,
    vararg aliases: String = emptyArray()
) {

    val formattedUsage = usage ?: ""
    val names = arrayOf(name, *aliases)

    open fun handleCallbackQuery(bot: TelegramBot, callbackQuery: CallbackQuery) {}

    fun exec(args: List<String>, bot: TelegramBot, user: User, chat: Long): A? {
        return try {
            this.creator.parseArgs(args).run {
                if (this != null) {
                    exec(this, bot, user, chat)
                }

                this
            }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    abstract fun exec(args: A, bot: TelegramBot, user: User, chatId: Long)
}

package fe.tgbotutil.command

interface CommandArgumentCreator<T> {
    fun parseArgs(args: List<String>): T
    fun requiredArgs(): Array<Int>

    fun hasEnoughArgs(args: Int): Boolean {
        return requiredArgs().contains(args)
    }
}

object EmptyArgument : CommandArgumentCreator<EmptyArgument> {
    override fun parseArgs(args: List<String>) = EmptyArgument
    override fun requiredArgs() = arrayOf(0)
}

package fe.tgbotutil

import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.request.InlineKeyboardButton
import java.util.*

object TelegramCallbackHandler {
    data class Callback(
        val chatId: Long, val messageId: Int, val handler: Map<String, CallbackHandler>,
        val createdAt: Long = System.currentTimeMillis()
    ) {
        fun match(query: CallbackQuery): Boolean {
            return query.message().chat().id() == chatId && query.message().messageId() == messageId
        }
    }

    private val list = LinkedList<Callback>()

    fun addCallback(callback: Callback) {
        synchronized(list) {
            list.add(callback)
        }
    }

    fun addCallback(chatId: Long, messageId: Int, handlers: Collection<IdCallbackHandler>) {
        synchronized(list) {
            list.add(Callback(chatId, messageId, handlers.associate { it.asPair }))
        }
    }

    fun addCallback(chatId: Long, messageId: Int, vararg handlers: IdCallbackHandler) {
        synchronized(list) {
            list.add(Callback(chatId, messageId, handlers.associate { it.asPair }))
        }
    }

    fun execute(query: CallbackQuery): Boolean {
        synchronized(list) {
            return list.find { it.match(query) }?.let { callback ->
                callback.handler[query.data()]?.invoke(query)
                list.remove(callback)
                true
            } ?: false
        }
    }
}


typealias CallbackHandler = (CallbackQuery) -> Unit

data class IdCallbackHandler(
    val id: String,
    val callbackHandler: CallbackHandler
) {
    val asPair = Pair(id, callbackHandler)
}

fun newCallbackHandler(id: String = UUID.randomUUID().toString(), callbackHandler: CallbackHandler): IdCallbackHandler {
    return IdCallbackHandler(id, callbackHandler)
}

fun IdCallbackHandler.toInlineButton(btnText: String): InlineKeyboardButton {
    return InlineKeyboardButton(btnText).callbackData(this.id)
}

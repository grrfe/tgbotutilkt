package fe.tgbotutil.ext

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.Keyboard
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove
import com.pengrad.telegrambot.request.SendMessage
import com.pengrad.telegrambot.response.SendResponse
import fe.tgbotutil.MarkdownStringBuilder

sealed class Message(val text: String, val parseMode: ParseMode?) {
    class MarkdownV2Message(markdown: MarkdownStringBuilder) : Message(markdown.build(), ParseMode.MarkdownV2)
    class TextMessage(text: String) : Message(text, null)
}

fun TelegramBot.sendMarkdownV2(
    chatId: Long,
    message: MarkdownStringBuilder,
    replyMarkup: Keyboard = ReplyKeyboardRemove()
) =
    this.sendMessage(chatId, Message.MarkdownV2Message(message), replyMarkup)

fun TelegramBot.sendText(chatId: Long, message: String, replyMarkup: Keyboard = ReplyKeyboardRemove()) =
    this.sendMessage(chatId, Message.TextMessage(message), replyMarkup)


fun <T : Message> TelegramBot.sendMessage(
    chatId: Long,
    message: T,
    replyMarkup: Keyboard = ReplyKeyboardRemove()
): SendResponse? {
    return this.execute(SendMessage(chatId, message.text).apply {
        message.parseMode?.let { this.parseMode(it) }
    }.replyMarkup(replyMarkup))
}

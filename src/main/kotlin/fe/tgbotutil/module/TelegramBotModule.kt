package fe.tgbotutil.module

import com.pengrad.telegrambot.TelegramBot
import fe.tgbotutil.config.TelegramConfig
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.koin.dsl.module
import java.io.IOException
import java.net.ProxySelector
import java.net.SocketAddress
import java.net.URI
import java.util.concurrent.TimeUnit

val telegramBotModule = module {
    single<TelegramBot> {
        val config = get<TelegramConfig>()
        val proxy = config.proxyConfig?.toProxy()
        config.proxyConfig?.setAsRequestAuth()

        val clientBuilder = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .pingInterval(3, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor { chain ->
                val request: Request = chain.request()
                var response: Response? = null
                var error: IOException? = IOException()
                try {
                    response = chain.proceed(request)
                } catch (e: IOException) {
                    error = e
                }

                var tryCount = 0
                while ((response == null || !response.isSuccessful) && tryCount < 6) {
                    try {
                        Thread.sleep(300)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    tryCount++
                    println("Retry $tryCount")
                    try {
                        response = chain.proceed(request)
                    } catch (e: IOException) {
                        error = e
                    }
                }

                return@addInterceptor response ?: throw error!!
            }



        val client = if (proxy != null) {
            clientBuilder.proxySelector(object : ProxySelector() {
                override fun select(uri: URI?) = mutableListOf(proxy, proxy, proxy)

                override fun connectFailed(uri: URI, sa: SocketAddress, ioe: IOException) {
                    println("Connect failed for $uri @ $sa")
                    ioe.printStackTrace()
                }
            }).build()
        } else clientBuilder.build()

        TelegramBot.Builder(config.botToken)
            .okHttpClient(client)
            .build()
    }
}


plugins {
    kotlin("jvm") version "1.9.24"
    java
    id("net.nemerosa.versioning") version "3.1.0"
}

group = "fe.tgbotutil"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    api(platform("com.github.1fexd:super"))
    api("com.github.pengrad:java-telegram-bot-api:7.4.0")
    api("io.insert-koin:koin-core")

    testImplementation(kotlin("test"))
}


